package Aps;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexao {
 
    private String driver = "com.mysql.jdbc.Driver";
    private String URL = "jdbc:mysql://162.241.203.20:3306/infinito_uni";
    private String USER = "infinito_uni";
    private String SENHA = "admin@001a";
    private Connection conn;
 
    public Conexao() throws SQLException, ClassNotFoundException {
        try {
            Class.forName(driver);
            conn = (Connection) DriverManager.getConnection(URL, USER, SENHA);
        } catch (SQLException | ClassNotFoundException e) {
            throw e;
        }
    }
 
    public Connection getConn() {
        return conn;
    }
 
    public void fecharConexao() throws SQLException {
        try {
            conn.close();
        } catch (SQLException e) {
            throw e;
        }
    }
}
