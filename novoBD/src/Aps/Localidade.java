package Aps;

public class Localidade {
	
	private int codLocal;
	private String nome;
	private String endereco;
	private String telefone;
	
	public Localidade() {
		
	}
	
	public Localidade(int codLocal, String nome, String endereco, String telefone) {
		this.codLocal = codLocal;
		this.nome = nome;
		this.endereco = endereco;
		this.telefone = telefone;
	}
	
	public int getCodLocal() {
		return codLocal;
	}
	
	public void setCodLocal(int codLocal) {
		this.codLocal = codLocal;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getEndereco() {
		return endereco;
	}
	
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}	
}
