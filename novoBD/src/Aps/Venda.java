package Aps;

import java.util.Date;

public class Venda {
	
	private Cliente cliente;
	private Produto produto;
	private Localidade local;
	private int qtdVenda;
	private double valorTotal;
	private Date dataVenda;
	
	public Venda() {
		
	}
	
	public Venda(Cliente cliente, Produto produto, Localidade local, int qtdVenda, double valorTotal, Date dataVenda) {
		this.cliente = cliente;
		this.produto = produto;
		this.local = local;
		this.qtdVenda = qtdVenda;
		this.valorTotal = valorTotal;
		this.dataVenda = dataVenda;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Localidade getLocal() {
		return local;
	}

	public void setLocal(Localidade local) {
		this.local = local;
	}

	public int getQtdVenda() {
		return qtdVenda;
	}

	public void setQtdVenda(int qtdVenda) {
		this.qtdVenda = qtdVenda;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Date getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}
}
