package Aps;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.sql.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.Date;

public class AppNovo extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel modelo = new DefaultTableModel();
	private String[] objRow = new String[2];
	private static Locale locale  = new Locale("pt", "BR");
	private static DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
	private static String pattern = "###,###.00";
	private static DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
	private JComboBox sltCliente;
	private JComboBox sltLocal;
	private JComboBox sltProduto;
    private JLabel lbTotal;
    private JTextField lbQuantidade;
    private JButton btVender;
    private JButton btExcluir;
    private boolean vendasLiberada = false;
    
    private Date date = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
 
    private Conexao conexao;
    private Statement stmt;
    
    List<Cliente> clientes = new ArrayList<Cliente>();
    List<Produto> produtos = new ArrayList<Produto>();
    List<Localidade> locais = new ArrayList<Localidade>();
    List<Venda> vendas = new ArrayList<Venda>();
    private JLabel msg;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		System.out.println("*** INICIANDO SISTEMA ***");
		System.out.println("Aguarde....");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppNovo frame = new AppNovo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AppNovo() {
		System.out.println("Carregando layout");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 425);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbTitulo = new JLabel("CAIXA LIVRE");
		lbTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitulo.setForeground(Color.BLUE);
		lbTitulo.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbTitulo.setBounds(144, 11, 143, 25);
		contentPane.add(lbTitulo);
		
		table = new JTable(modelo);
		table.setBounds(10, 202, 413, 139);
		contentPane.add(table);		

        modelo.addColumn("Produto");
        modelo.addColumn("Quantidade");
        modelo.addColumn("PrUnit");
        modelo.addColumn("VlTotal");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(0, 297, 434, -91);
		contentPane.add(scrollPane);
		
		sltCliente = new JComboBox();
		sltCliente.setBounds(10, 82, 171, 20);
		contentPane.add(sltCliente);
		
		sltCliente.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	alternarCliente();
		    }
		});
		
		JLabel lblClienteSelecionado = new JLabel("Cliente Selecionado:");
		lblClienteSelecionado.setBounds(11, 62, 124, 14);
		contentPane.add(lblClienteSelecionado);
		
		sltLocal = new JComboBox();
		sltLocal.setBounds(191, 82, 124, 20);
		contentPane.add(sltLocal);
		
		sltLocal.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	alternarLocal();
		    }
		});
		
		JLabel lblLocalDeVenda = new JLabel("Local de venda:");
		lblLocalDeVenda.setBounds(191, 62, 124, 14);
		contentPane.add(lblLocalDeVenda);
		
		sltProduto = new JComboBox();
		sltProduto.setBounds(11, 135, 170, 20);
		contentPane.add(sltProduto);
		
		JLabel lblCdigoDoProduto = new JLabel("Produto:");
		lblCdigoDoProduto.setBounds(11, 113, 124, 14);
		contentPane.add(lblCdigoDoProduto);
		
		lbQuantidade = new JTextField();
		lbQuantidade.setBounds(191, 135, 124, 20);
		contentPane.add(lbQuantidade);
		lbQuantidade.setColumns(10);
		
		JLabel lblQuantidade = new JLabel("Quantidade:");
		lblQuantidade.setBounds(191, 113, 86, 14);
		contentPane.add(lblQuantidade);
		
		btVender = new JButton("Vender");
		btVender.setFont(new Font("Tahoma", Font.BOLD, 11));
		btVender.setForeground(new Color(0, 128, 0));
		btVender.setBounds(326, 134, 98, 23);
		contentPane.add(btVender);
		
		btVender.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	try {
					vender();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    }
		});
		
		btExcluir = new JButton("Excluir");
		btExcluir.setForeground(new Color(128, 0, 0));
		btExcluir.setFont(new Font("Tahoma", Font.BOLD, 11));
		btExcluir.setBounds(326, 168, 98, 23);
		contentPane.add(btExcluir);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(67, 47, 304, 2);
		contentPane.add(separator);
		
		JLabel infoTotal = new JLabel("TOTAL:");
		infoTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		infoTotal.setBounds(254, 364, 46, 14);
		contentPane.add(infoTotal);
		
		lbTotal = new JLabel();
		lbTotal.setHorizontalAlignment(SwingConstants.LEFT);
		lbTotal.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbTotal.setForeground(new Color(0, 100, 0));
		lbTotal.setBounds(310, 364, 113, 14);
		contentPane.add(lbTotal);
		
		msg = new JLabel("");
		msg.setHorizontalAlignment(SwingConstants.CENTER);
		msg.setFont(new Font("Tahoma", Font.BOLD, 11));
		msg.setForeground(new Color(139, 0, 0));
		msg.setBounds(10, 166, 305, 20);
		contentPane.add(msg);
		
		btExcluir.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	try {
					excluir();
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    }
		});
		try {
			carregaClientes();
			carregaLocais();
	    	Item item = (Item)sltCliente.getSelectedItem();
	    	int idCliente = item.getId();
	    	carregaVendas(idCliente);
			System.out.println("SISTEMA PRONTO");
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void vender() throws SQLException, ClassNotFoundException {
		mostrarMSG("processando...");
		conexao = new Conexao();
		
		Item itemProd = (Item)sltProduto.getSelectedItem();
		int idProduto = itemProd.getId();
		
		Item itemCli = (Item)sltCliente.getSelectedItem();
		int idCliente = itemCli.getId();
		
		Item itemLocal = (Item)sltCliente.getSelectedItem();
		int idLocal = itemLocal.getId();		
		
		int quantVend;
		double valorTotal;
		double valorUnit;
		Produto p = new Produto();
		p = procurarProduto(idProduto);
		
		//VERIFICAR ESTOQUE PRODUTO
		try {
			quantVend = Integer.parseInt(lbQuantidade.getText());
			if(p.getQtdEstoque() < quantVend) {
				mostrarMSG("Sem estoque");
			}else{
				valorUnit = p.getPrecoUnitario();
				valorTotal = valorUnit * quantVend;
				//ADICIONAR VENDA NO ARRAY LIST
				Venda venda = new Venda();
				venda.setCliente(procurarCliente(idCliente));
				venda.setLocal(procurarLocal(idLocal));
				venda.setProduto(procurarProduto(idProduto));
				venda.setQtdVenda(quantVend);
				venda.setValorTotal(valorTotal);
				venda.setDataVenda(date);

                vendas.add(venda);
				
				//ADICIONAR VENDA NO BD
	            stmt = (Statement) conexao.getConn().createStatement();
	            ResultSet rs = stmt.executeQuery("SELECT QTD_VENDA, VALOR_TOTAL FROM VENDA WHERE CODCLI = "+idCliente+" AND CODPROD = "+idProduto+" AND CODLOCAL = "+idLocal);
	            int size = 0;
	            if (rs != null) {
	                while (rs.next()) {
	                	quantVend += rs.getInt("QTD_VENDA");
	                	valorTotal += rs.getDouble("VALOR_TOTAL");
	                	size++;
	                }
	            }
	            if(size != 0){
	            	stmt.executeUpdate("UPDATE VENDA SET QTD_VENDA = " 
                            + quantVend + ", VALOR_TOTAL = "
                            + valorTotal + " WHERE CODCLI = "
                            + idCliente + " AND CODPROD = "
                            + idProduto + " AND CODLOCAL = "
                            + idLocal);
	            }else{
					stmt.executeUpdate("INSERT INTO VENDA (CODCLI, CODPROD, CODLOCAL, QTD_VENDA, VALOR_TOTAL, DATA_VENDA) VALUES (" 
	                        + idCliente + ",'"
	                        + idProduto + "','"
	                        + idLocal + "','"
	                        + quantVend + "','"
	                        + valorTotal + "','"
	                        + formatter.format(date) + "')");
	            }
				
				//ATUALIZAR TABELA SWING
				carregaVendas(idCliente);
				
			}
		} catch (NumberFormatException nfe) {
	    	mostrarMSG("Dados incorreto");
	    } finally {
            conexao.fecharConexao();
            mostrarMSG("");
        }	
	}
	
	public void excluir() throws SQLException, ClassNotFoundException {
		System.out.println("Excluindo vendas");
		vendas.clear();
		zerarTabela();
		conexao = new Conexao();
		
		Item item = (Item)sltCliente.getSelectedItem();
		int idCliente = item.getId();
        
        try {
            stmt = (Statement) conexao.getConn().createStatement();
            stmt.executeUpdate("DELETE FROM VENDA WHERE CODCLI = " + idCliente);           
         } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conexao.fecharConexao();
        }
		
	}
	
	public void alternarCliente() {
    	Item item = (Item)sltCliente.getSelectedItem();
    	int idCliente = item.getId();
    	try {
    		if(vendasLiberada) {
    			carregaVendas(idCliente);
    		}
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	public void alternarLocal() {
    	Item item = (Item)sltLocal.getSelectedItem();
    	int idLocal = item.getId();
    	try {
			carregaProdutos(idLocal);
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void carregaClientes() throws SQLException, ClassNotFoundException {
		System.out.println("Carregando clientes");
		
		sltCliente.removeAllItems();
		clientes.clear();
		conexao = new Conexao();
        
        try {
            stmt = (Statement) conexao.getConn().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM CLIENTE WHERE STATUS = 'A' ORDER BY NOME");
            while (rs.next()) {
                Cliente cliente = new Cliente();                
                cliente.setNome(rs.getString("NOME"));
                cliente.setBonus(rs.getInt("BONUS"));
                cliente.setCodigoCliente(rs.getInt("CODCLI"));
                cliente.setPerfil(rs.getString("PERFIL"));
                clientes.add(cliente);
                sltCliente.addItem(new Item(rs.getInt("CODCLI"), rs.getString("NOME")));
            }
            
         } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conexao.fecharConexao();
        }
        
	}
	
	public void carregaLocais() throws SQLException, ClassNotFoundException {
		System.out.println("Carregando locais");
		sltLocal.removeAllItems();
		locais.clear();
		conexao = new Conexao();
        
        try {
            stmt = (Statement) conexao.getConn().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM LOCALIDADE ORDER BY NOME");
            while (rs.next()) {
                Localidade local = new Localidade();                
                local.setCodLocal(rs.getInt("CODLOCAL"));
                local.setNome(rs.getString("NOME"));
                local.setEndereco(rs.getString("ENDERECO"));
                local.setTelefone(rs.getString("TELEFONE"));
                locais.add(local);
                sltLocal.addItem(new Item(rs.getInt("CODLOCAL"), rs.getString("NOME")));
            }
            
         } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conexao.fecharConexao();
        }
	}
	
	public void carregaProdutos(int local) throws SQLException, ClassNotFoundException {
		System.out.println("Carregando produtos");
		sltProduto.removeAllItems();
		produtos.clear();
		conexao = new Conexao();
        
        try {
            stmt = (Statement) conexao.getConn().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUTO WHERE CODLOCAL = "+local+" ORDER BY DESCRICAO");
            while (rs.next()) {
                Produto produto = new Produto();
                produto.setCodProd(rs.getInt("CODPROD"));
                produto.setDescricao(rs.getString("DESCRICAO"));
                produto.setQtdEstoque(rs.getInt("QTD_ESTOQUE"));
                produto.setPrecoUnitario(rs.getDouble("PRECO_UNITARIO"));
                produtos.add(produto);
                sltProduto.addItem(new Item(rs.getInt("CODPROD"), rs.getString("DESCRICAO")));
            }
            
         } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conexao.fecharConexao();
            vendasLiberada = true;
        }
	}
	
	public void carregaVendas(int cliente) throws SQLException, ClassNotFoundException {
		
		System.out.println("Carregando itens do cliente");
		conexao = new Conexao();
		vendas.clear();
		zerarTabela();
		int idProduto;
		int idLocal;
		double valorParc;
		double total = 0;
        
        try {
            stmt = (Statement) conexao.getConn().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM VENDA WHERE CODCLI = "+cliente);
            
            while (rs.next()) {
                Venda venda = new Venda();
                
                valorParc = rs.getDouble("VALOR_TOTAL");
            	
            	idProduto = rs.getInt("CODPROD");
            	idLocal = rs.getInt("CODLOCAL");
            	
            	venda.setQtdVenda(rs.getInt("QTD_VENDA"));
            	venda.setValorTotal(valorParc);
            	venda.setDataVenda(rs.getDate("DATA_VENDA"));

            	venda.setCliente(procurarCliente(cliente));            	
            	venda.setProduto(procurarProduto(idProduto));            	
            	venda.setLocal(procurarLocal(idLocal));

                vendas.add(venda);
                criarLinha(venda);
                
                total += valorParc;
                
            }
            if(total == 0) {
            	lbTotal.setText("R$ 0,00");
            }else {
            	lbTotal.setText("R$ "+decimalFormat.format(total));
            }
            
            
         } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conexao.fecharConexao();
        }
	}
	
	public Cliente procurarCliente(int id){
		for (Cliente c: clientes) {
			if(c.getCodigoCliente() == id) {
				System.out.println("Cliente localizado");
				return c;
			}
		}
		System.out.println("Cliente N�O localizado");
		return null;
	}
	
	public Localidade procurarLocal(int id){
		for (Localidade c: locais) {
			if(c.getCodLocal() == id) {
				System.out.println("Local localizado");
				return c;
			}
		}
		System.out.println("Local N�O localizado");
		return null;
	}
	
	public Produto procurarProduto(int id){
		for (Produto c: produtos) {
			if(c.getCodProd() == id) {
				System.out.println("Produto localizado");
				return c;
			}
		}
		System.out.println("Produto N�O localizado");
		return null;
	}
	
	private void zerarTabela() {
		modelo.setRowCount(0);
		lbTotal.setText("");
	}
	
	public void criarLinha(Venda venda) {
		double valorTotal;
		valorTotal = venda.getProduto().getPrecoUnitario() * venda.getQtdVenda();
		modelo.addRow(new Object[]{venda.getProduto().getDescricao(), venda.getQtdVenda(),"R$ "+decimalFormat.format(venda.getProduto().getPrecoUnitario()), "R$ "+decimalFormat.format(valorTotal)});
	}	
	private void mostrarMSG(String mensagem) {
		msg.setText(mensagem);
	}
}
class Item {
    private int id;
    private String description;

    public Item(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String toString() {
        return description;
    }
}