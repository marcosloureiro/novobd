package Aps;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Cliente {
	
	private int codigoCliente;
	private String nome;
	private int bonus;
	private String perfil;
	private String status;
	
	public Cliente() {
		
	}
	
	public Cliente(int codigoCliente, String nome, int bonus, String perfil, String status) {
		this.codigoCliente = codigoCliente;
		this.nome = nome;
		this.bonus = bonus;
		this.perfil = perfil;
		this.status = status;
	}
	
	public int getCodigoCliente() {
		return codigoCliente;
	}
	
	public void setCodigoCliente(int codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getBonus() {
		return bonus;
	}
	
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}
	
	public String getPerfil() {
		return perfil;
	}
	
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
}
