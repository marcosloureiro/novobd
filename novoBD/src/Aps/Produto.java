package Aps;

public class Produto {
	
	private int codProd;
	private Localidade local;
	private String descricao;
	private int qtdEstoque;
	private double precoUnitario;
	
	public Produto() {
		
	}
	
	public Produto(int codProd, Localidade local, String descricao, int qtdEstoque, double precoUnitario) {
		this.codProd = codProd;
		this.local = local;
		this.descricao = descricao;
		this.qtdEstoque = qtdEstoque;
		this.precoUnitario = precoUnitario;
	}

	public int getCodProd() {
		return codProd;
	}

	public void setCodProd(int codProd) {
		this.codProd = codProd;
	}

	public Localidade getLocal() {
		return local;
	}

	public void setLocal(Localidade local) {
		this.local = local;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getQtdEstoque() {
		return qtdEstoque;
	}

	public void setQtdEstoque(int qtdEstoque) {
		this.qtdEstoque = qtdEstoque;
	}

	public double getPrecoUnitario() {
		return precoUnitario;
	}

	public void setPrecoUnitario(double precoUnitario) {
		this.precoUnitario = precoUnitario;
	}
}
