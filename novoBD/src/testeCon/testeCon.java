package testeCon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class testeCon{

	public static void main(String[] args) {
		
		try {
			
            Class.forName("com.mysql.jdbc.Driver");
            
            String strCon = "jdbc:mysql://162.241.203.20:3306/infinito_uni";
	        Connection con = DriverManager.getConnection(strCon, "infinito_uni", "admin@001a");

            Statement stmt = con.createStatement();

            int resultado = stmt.executeUpdate("INSERT INTO banco (numeroConta, saldo, tipo) VALUES (2120, '50', 'corrente');");
            
            if(resultado > 0) {
            	System.out.println("Inserido com sucesso!");
            }
            ResultSet rs = stmt.executeQuery("SELECT * FROM banco");
            
            while (rs.next()){
                  System.out.println("N�mero: " + rs.getString("numeroConta") 
                  + " - " + "Saldo: " + rs.getShort("saldo"));
            }

            con.close();
            rs.close();
            stmt.close();
            
        } catch (ClassNotFoundException ex1) {
            System.out.println("Classe n�o encontrada");
        } catch (SQLException ex3) {
        	System.out.println(ex3);
        }

	}

}
